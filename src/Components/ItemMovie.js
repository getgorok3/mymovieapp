import React from 'react';
import { Card, Icon, Avatar } from 'antd';
import TextTruncate from 'react-text-truncate';
import { connect } from 'react-redux'; //redux
const { Meta } = Card;

const mapDispatchToProps = dispatch => {
  //redux
  return {
    onItemMovieClick: item =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  };
};

function ItemMovie(props) {
  const item = props.item;
  return (
    <Card
      onClick={() => {
        props.onItemMovieClick(item); //เรียกฟังชั่นใน dispatch ผ่าน props //redux
      }}
      hoverable
      cover={<img alt="example" src={item.image_url} />}
    >
      <Meta
        title={item.title}
        description={
          <TextTruncate
            line={1}
            truncateText="..."
            text={item.overview}
            textTruncateChild={<a hred="#">Read more</a>}
          />
        }
      />
    </Card>
  );
}
export default connect(
  //redux
  null,
  mapDispatchToProps
)(ItemMovie);
// function 2 parameter == 1. map state to props and 2. map dispatch to props
