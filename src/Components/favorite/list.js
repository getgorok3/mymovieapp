import React, { Component } from 'react';
import { List } from 'antd';
import ItemFavorite from './item';
class ListFavorite extends Component {
  state = {
    items: []
  };
  componentDidMount() {
    const jsonStr = localStorage.getItem('list-fav'); //โหลดของเดิมมาเซ็ต array กับ ค่า null อีกวิธี
    if (jsonStr) {
      const items = JSON.parse(jsonStr);
      this.setState({ items });
    }
  }

  render() {
    console.log('fav item', this.state.items);
    return (
      <div style={{ height: '100vh' }}>
        <List
          grid={{ gutter: 16, column: 4 }}
          dataSource={this.state.items}
          renderItem={item => {
            console.log('render', item);
            return (
              <List.Item>
                <ItemFavorite
                  item={item}
                  onItemMovieClick={this.props.onItemMovieClick}
                />
              </List.Item>
            );
          }}
        />
      </div>
    );
  }
}

export default ListFavorite;
