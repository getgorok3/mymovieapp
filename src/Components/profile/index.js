import React, { Component } from 'react';
import { Modal } from 'antd';

const KEY_USER_DATA = 'user_data';

class Profile extends Component {
  state = {
    email: '',
    isShowDialog: false
  };

  showDialogConfirmLogout = () => {
    this.setState({ isShowDialog: true });
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    this.setState({ email: JSON.parse(jsonStr).email });
  }
  handleOk = () => {
    localStorage.setItem(
      KEY_USER_DATA,
      JSON.stringify({
        isLoggedIn: false
      })
    );
    this.props.history.push('/');
  };
  handleCancel = () => {
    this.setState({ isShowDialog: false });
  };
  render() {
    return (
      <div>
        <h1>Email: {this.state.email}</h1>
        <button
          style={{
            padding: '10px 20px 10px 20px',
            background: 'red', //padding ซ้าย บน ขวา ล่าง
            color: 'white',
            fontSize: '18px',
            cursor: 'pointer',
            border: 'none',
            borderRadius: '10px'
          }}
          onClick={this.showDialogConfirmLogout}
        >
          Logout
        </button>

        <Modal
          title="Logout"
          visible={this.state.isShowDialog}
          onOk={this.handleOk}
          onCancel={this.handleCancel}
        >
          <p>Are you sure to logout?</p>
        </Modal>
      </div>
    );
  }
}
export default Profile;
