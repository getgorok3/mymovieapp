import React, { Component } from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import { withRouter } from 'react-router-dom';
import * as constants from '../ConstantValue';

const KEY_USER_DATA = 'user_data';
class Login extends Component {
  state = {
    // state = object to keep variable
    id: '', // no need to re render = auto change
    password: ''
  };

  onEmailChange = e => {
    const email = e.target.value; // get value from input
    this.setState({ id: email }); // setState must receive an object
    // this.setState({email})  if the name not dif we can set like this
    // warning camt if log it will show cam
    // we need function call back id: email, () => { console.log(this.state.email)} === camt
  };

  onPasswordChange = e => {
    const password = e.target.value;
    this.setState({ password });
  };

  validateEmail(email) {
    const emailPlattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emailPlattern.test(String(email).toLowerCase());
  }

  validatePassword(password) {
    const passwordPlattern = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9]).{8,}$/;
    return passwordPlattern.test(String(password));
  }

  navigateToMainPage = () => {
    const { history } = this.props;
    history.push(constants.MOVIES_PATH);
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem(KEY_USER_DATA);
    const isLoggedIn = jsonStr && JSON.parse(jsonStr).isLoggedIn;
    if (isLoggedIn) {
      // this.navigateToMainPage();
    }
  }

  onSubmitFormLogin = e => {
    e.preventDefault();
    const isValid = this.validateEmail(this.state.id);
    const isValidPass = this.validatePassword(this.state.password);
    const { history } = this.props;
    if (isValid && isValidPass) {
      // console.log(`Email: ${this.state.id}`);
      // console.log(`Password: ${this.state.password}`);
      localStorage.setItem(
        KEY_USER_DATA,
        JSON.stringify({
          isLoggedIn: true,
          email: this.state.id
        })
      );
      this.navigateToMainPage();
    } else {
      message.error('Email or password is invalid', 1);
    }
    // console.log('Email', this.state.id);                 || show result ex
    // console.log('Password', this.state.password);
  };
  render() {
    return (
      <div>
        <Form onSubmit={this.onSubmitFormLogin}>
          <Form.Item>
            <h2>Login</h2>
            <Input
              prefix={<Icon type="user" />}
              placeholder="Email"
              onChange={this.onEmailChange}
            />
          </Form.Item>

          <Form.Item>
            <Input
              prefix={<Icon type="key" />}
              placeholder="Password"
              type="password"
              onChange={this.onPasswordChange}
            />
          </Form.Item>

          <Form.Item>
            <Button type="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withRouter(Login);
