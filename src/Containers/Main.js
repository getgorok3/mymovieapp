import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import { Spin, Modal, Button, Layout, Menu, message } from 'antd';
import RouteMenu from './RouteMenu';
import { connect } from 'react-redux'; //redux

const { Header, Content, Footer } = Layout;
const menus = ['movies', 'favorite', 'profile'];

//redux
const mapStateToProps = state => {
  return {
    isShowDialog: state.isShowDialog,
    itemMovieClick: state.itemMovieDetail
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onDismissDialog: () =>
      dispatch({
        type: 'dismiss_dialog'
      }),
    onItemMovieClick: (
      item //Throw to store
    ) =>
      dispatch({
        type: 'click_item',
        payload: item
      })
  };
};

class Movies extends Component {
  state = {
    items: [],
    isShowModal: false,
    itemMovie: null,
    pathName: menus[0],
    favItems: []
  };

  // onItemMovieClick = item => {     Takeover by redux
  //   // call back throw props and inside call back again
  //   // TODO: Show detail movie modal
  //   this.setState({ isShowModal: true, itemMovie: item }); //Throw it back to parent === main
  // }; // reuse but can be  call back hell

  onModalClickOK = () => {
    // this.setState({ isShowModal: false });
    this.props.onDismissDialog();
  };
  onModalClickCancel = () => {
    // this.setState({ isShowModal: false });
    this.props.onDismissDialog();
  };

  onMenuClick = e => {
    var path = '/';
    if (e.key != 'home') {
      //ดึง key ของ event ออกมา
      path = `/${e.key}`; // หน้าไหนไม่ = home
    }
    this.props.history.replace(path);
    //TODO not change path == save path to state **************
  };

  //TODO not change path == save path to state   ****************
  // switchContent = () => {
  //     if(this.state.path === menus[0]){
  //         return (
  //            Component Lis movies
  //         )
  //     }else   if(this.state.path === menus[1]){
  //         return (
  //             Component Lis fav
  //         )
  //     }  if(this.state.path === menus[2]){
  //         return (
  //             Component index
  //     }
  // }

  onClickFavorite = () => {
    const itemClick = this.props.itemMovieClick;
    const items = this.state.favItems; // ***save ได้แต่ยังไม่เข้า f5 ละadd ได้
    const result = items.find(item => {
      //แก้ ปัญหา รีเฟชรหาย ใช้ find รับ func
      return item.title === itemClick.title; // return obj
    });
    if (result) {
      message.error('Already added to your favorites', 1);
      this.onModalClickCancel();
    } else {
      items.push(itemClick);
      localStorage.setItem('list-fav', JSON.stringify(items)); //เก็บ เปลี่ยน ให้ เป็น string ก่อน
      message.success('Added to your favorite movie success', 1);
      this.onModalClickCancel();
    }
  };

  componentDidMount() {
    const jsonStr = localStorage.getItem('list-fav');
    const items = JSON.parse(jsonStr) || []; //เปลี่ยน string > array
    this.setState({ favItems: items }); //เก็บเข้าใส่ state ใหม่
    // favItems เป็น [] แต่ ถ้า items ว่าง null จะ พัง จึงต้องใส่ || [] หรือ jsonStr &&

    const { pathname } = this.props.location; //  บอก โลเคชั่น เอา pathname ออกมา
    var pathName = menus[0]; // ตัวแปลใหม่ ให้ = movies
    if (pathname != '/') {
      pathName = pathname.replace('/', ''); //เปบี่ยน /something เป็น 'something'
      if (!menus.includes(pathName)) pathName = menus[0]; //แ้ำแา member ของ array menus มี path นี้ไหม
    } //ถ้าไม่มี set เป็น /movies
    this.setState({ pathName }); //ถ้ามีใน array เอาค่านั้น
    fetch('https://workshopup.herokuapp.com/movie')
      .then(response => response.json()) // obj response => json(promise) => show
      .then(items => this.setState({ items: items.results })); // set in state for this class can used
  }

  render() {
    const item = this.props.itemMovieClick;
    // console.log("items", this.state.items)
    return (
      <div>
        {this.state.items.length > 0 ? (
          // <ListMovies items={this.state.items}
          //     onItemMovieClick={this.onItemMovieClick} />        //items= can set to any name
          <div style={{ height: '100vh' }}>
            {''}
            <Layout className="layout" style={{ background: 'white' }}>
              <Header
                style={{
                  padding: '0px', //การยืดขอบ
                  position: 'fixed',
                  zIndex: 1, //ทำให้อยู่ข้างบน fixไว้ ===  แกนZ
                  width: '100%'
                }}
              >
                <Menu
                  theme="light"
                  mode="horizontal"
                  defaultSelectedKeys={[this.state.pathName]}
                  style={{ lineHeight: '64px' }}
                  onClick={e => {
                    this.onMenuClick(e);
                  }}
                >
                  <Menu.Item key={menus[0]}> Home</Menu.Item>
                  <Menu.Item key={menus[1]}> Favorite</Menu.Item>
                  <Menu.Item key={menus[2]}> Profile</Menu.Item>
                </Menu>
              </Header>
              <Content
                style={{
                  padding: '16px', //padding หดลงแค่ text
                  marginTop: 64, //margin ดึงลงมาทั้งกรอบ
                  minHeight: '600px',
                  justifyContent: 'center',
                  alignItems: 'center',
                  display: 'flex'
                }}
              >
                <RouteMenu
                  items={this.state.items}
                  // onItemMovieClick={this.onItemMovieClick} used redux deleteit
                />
                {/* {this.switchContain  ************** */}
              </Content>
              <Footer style={{ textAlign: 'center', background: 'white' }}>
                Movie Application workshop @ CAMT
              </Footer>
            </Layout>
          </div>
        ) : (
          <Spin tip="Loading..." size="large" />
        )}
        {item != null ? (
          <Modal
            width="40%"
            style={{ maxHeight: '70%', top: '2%' }}
            title={item.title}
            visible={this.props.isShowDialog}
            onCancel={this.onModalClickCancel}
            footer={[
              <Button
                key="submit"
                type="primary"
                icon="heart"
                size="large"
                shape="circle"
                onClick={this.onClickFavorite}
              />,
              <Button
                key="submit2"
                type="primary"
                icon="shopping-cart"
                size="large"
                shape="circle"
                onClick={this.onClickButTicket}
              />
            ]}
          >
            <img src={item.image_url} style={{ width: '100%' }} />
            <br />
            <br />
            <p>{item.overview}</p>
          </Modal>
        ) : (
          <div />
        )}
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Movies);
