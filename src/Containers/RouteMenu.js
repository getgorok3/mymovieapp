import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import ListMovie from '../Components/ListMovies';
import * as constants from '../ConstantValue';
import ListFavorite from '../Components/favorite/list';
import Profile from '../Components/profile/'; // มันจะเข้าไปหา file index ก่อน

function RouteMenu(props) {
  return (
    <Switch>
      <Route
        path="/movies"
        exact
        render={() => {
          return <ListMovie items={props.items} />;
        }}
      />
      <Route path="/favorite" exact component={ListFavorite} />
      <Route path="/profile" exact component={Profile} />
      <Redirect from="/*" exact to="/" />{' '}
      {/* นอกเหนือจากที่เรากำหนดจะ redirect ไป home*/}
    </Switch>
  );
}

export default RouteMenu;
