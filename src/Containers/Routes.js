import { Route, Switch } from 'react-router-dom';
import React from 'react';
import Loginpage from './Login';
import Mainpage from './Main';
import * as constants from '../ConstantValue';

function Routes() {
  return (
    <div style={{ width: '100%' }}>
      <Switch>
        {' '}
        {/* ดังนั้นเอา Switch มาครอบกัน render ทั้งหมด*/}
        <Route exact path="/" component={Loginpage} />
        <Route component={Mainpage} />
      </Switch>
    </div>
  );
}

export default Routes;
