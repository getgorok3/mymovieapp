export default (
  state = {
    itemMovieDetail: {},
    isShowDialog: false
  },
  action
) => {
  switch (
    action.type //reducers accept and change item and sent
  ) {
    case 'click_item':
      return {
        isShowDialog: true,
        itemMovieDetail: action.payload //throw data through payload
      };
    case 'dismiss_dialog':
      return {
        isShowDialog: false,
        itemMovieDetail: {}
      };
    default:
      return state;
  }
};
